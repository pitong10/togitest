package com.eclipse.togtest.utils

import com.eclipse.togtest.models.DataModel

interface FragmentCallback {
    fun onDataSent(submittedData: DataModel?)
}