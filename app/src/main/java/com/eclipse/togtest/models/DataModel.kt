package com.eclipse.togtest.models

import android.net.Uri
import java.util.Date

data class DataModel(
    var name: String,
    var address: String,
    var birthday: String,
    var height: String,
    var weight: String,
    var photo: Uri
)