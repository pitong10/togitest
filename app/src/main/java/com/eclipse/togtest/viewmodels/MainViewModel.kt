package com.eclipse.togtest.viewmodels

import android.net.Uri
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import com.eclipse.togtest.models.DataModel

class MainViewModel(state : SavedStateHandle) : ViewModel() {
    // Keep the key as a constant
    companion object {
        private val USER_KEY = "userId"
    }

    private val savedStateHandle = state

    fun saveCurrentData(dataModel: DataModel) {
        // Sets a new value for the object associated to the key.
        savedStateHandle.set(USER_KEY, dataModel)
    }

    fun getCurrentData(): DataModel {
        // Gets the current value of the user id from the saved state handle
        return savedStateHandle.get(USER_KEY)?: DataModel("-","-","-","-","-",Uri.EMPTY)
    }
}