package com.eclipse.togtest.fragments

import android.R.attr
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.bumptech.glide.Glide
import com.eclipse.togtest.MainActivity
import com.eclipse.togtest.R
import com.eclipse.togtest.databinding.FragmentAddDataBinding
import com.eclipse.togtest.models.DataModel
import com.eclipse.togtest.utils.FragmentCallback
import com.theartofdev.edmodo.cropper.CropImage
import kotlinx.android.synthetic.main.fragment_preview_data.*
import android.app.Activity.RESULT_OK
import android.app.DatePickerDialog
import android.os.CountDownTimer
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit


class AddDataFragment : Fragment() {

    private var _binding: FragmentAddDataBinding? = null
    private val binding get() = _binding!!
    private var datePickerDialog: DatePickerDialog? = null
    private var dateFormatter: SimpleDateFormat? = null
    private lateinit var activity: MainActivity
    private var fragmentCallback: FragmentCallback? = null
    private var imagePath: Uri = Uri.EMPTY
    private var isSent = false
    private lateinit var dataModel:DataModel

    private val timer = object: CountDownTimer(60000, 1000) {
        override fun onTick(millisUntilFinished: Long) {
            binding.tvTimer.text = String.format("%02d:%02d",
                TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished)%60,
                TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) % 60
            )
        }

        override fun onFinish() {
            binding.btnSubmit.isEnabled = false
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            val result = CropImage.getActivityResult(data)
            if (resultCode == RESULT_OK) {
                imagePath = result.uri
                Glide.with(binding.root.context).load(imagePath).into(binding.civPhoto)
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                val error = result.error
                Toast.makeText(binding.root.context, "Fail to get image, details : ${error.message}", Toast.LENGTH_LONG).show()
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentAddDataBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroy() {
        super.onDestroy()
        if (isSent && fragmentCallback != null) {
            fragmentCallback?.onDataSent(dataModel)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        timer.start()
        dateFormatter = SimpleDateFormat("dd-MM-yyyy", Locale.US)

        activity = getActivity() as MainActivity
        activity.supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        binding.tieBirthDay.setOnClickListener {
            showDateDialog()
        }

        binding.layoutUploadImage.setOnClickListener {
            CropImage.activity()
                .setAspectRatio(1, 1)
                .start(binding.root.context, this)
        }

        binding.btnSubmit.setOnClickListener {
            if (!isFormValid()) {
                Toast.makeText(binding.root.context, getString(R.string.txt_input_warning), Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            val name = binding.tieName.text.toString().trim()
            val address = binding.tieAddress.text.toString().trim()
            val birthday = binding.tieBirthDay.text.toString().trim()
            val height = binding.tieHeight.text.toString().trim()
            val weight = binding.tieWeight.text.toString().trim()

            dataModel = DataModel(name, address, birthday, height, weight, imagePath)
            isSent = true
            activity.supportFragmentManager.popBackStackImmediate()
        }
    }

    private fun isFormValid(): Boolean{
        if (binding.tieName.text.isNullOrEmpty())
            return false
        if (binding.tieAddress.text.isNullOrEmpty())
            return false
        if (binding.tieBirthDay.text.isNullOrEmpty())
            return false
        if (binding.tieHeight.text.isNullOrEmpty())
            return false
        if (binding.tieWeight.text.isNullOrEmpty())
            return false
        if (imagePath == null)
            return false

        return true
    }

    fun setFragmentCallback(callback: FragmentCallback?) {
        this.fragmentCallback = callback
    }

    private fun showDateDialog() {
        val newCalendar = Calendar.getInstance()
        datePickerDialog = DatePickerDialog(binding.root.context,
            { view, year, monthOfYear, dayOfMonth ->
                val newDate = Calendar.getInstance()
                newDate[year, monthOfYear] = dayOfMonth
                binding.tieBirthDay.setText(dateFormatter!!.format(newDate.time))
            },
            newCalendar[Calendar.YEAR],
            newCalendar[Calendar.MONTH],
            newCalendar[Calendar.DAY_OF_MONTH]
        )

        datePickerDialog!!.show()
    }

}