package com.eclipse.togtest.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import com.bumptech.glide.Glide
import com.eclipse.togtest.MainActivity
import com.eclipse.togtest.databinding.FragmentPreviewDataBinding
import com.eclipse.togtest.models.DataModel
import com.eclipse.togtest.utils.FragmentCallback
import com.eclipse.togtest.utils.FragmentChangeListener

class PreviewDataFragment : Fragment(), FragmentCallback {

    private lateinit var activity: MainActivity
    private var _binding: FragmentPreviewDataBinding? = null
    private val binding get() = _binding!!
    lateinit var listeners: FragmentChangeListener

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentPreviewDataBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity = getActivity() as MainActivity
        activity.supportActionBar!!.setDisplayHomeAsUpEnabled(false)

        listeners = (activity as FragmentChangeListener?)!!
        binding.btnInput.setOnClickListener {
            val fragment = AddDataFragment()
            fragment.setFragmentCallback(this)
            listeners.replaceFragment(fragment)
        }
    }

    override fun onDataSent(submittedData: DataModel?) {
        Glide.with(binding.root.context).load(submittedData?.photo).into(binding.ivPhoto)
        binding.tvName.text = submittedData?.name
        binding.tvAddress.text = submittedData?.address
        binding.tvBrithday.text = submittedData?.birthday
        binding.tvHeight.text = submittedData?.height
        binding.tvWeight.text = submittedData?.weight
    }
}