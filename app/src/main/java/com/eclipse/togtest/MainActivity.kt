package com.eclipse.togtest

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.eclipse.togtest.databinding.ActivityMainBinding
import com.eclipse.togtest.fragments.PreviewDataFragment
import com.eclipse.togtest.models.DataModel
import android.os.Looper
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.eclipse.togtest.fragments.AddDataFragment
import com.eclipse.togtest.utils.FragmentChangeListener





class MainActivity : AppCompatActivity() , FragmentChangeListener {

    private lateinit var binding: ActivityMainBinding
    private var doubleBackToExitPressedOnce = false
    lateinit var data: DataModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        showMainFragment()
    }

    private fun showMainFragment() {
        val fragment = PreviewDataFragment()
        supportFragmentManager.beginTransaction()
            .replace(binding.fragmentContainer.id, fragment)
            .commit()
    }

    override fun onBackPressed() {
        val count = supportFragmentManager.backStackEntryCount
        if (count == 0) {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed()
                return
            }

            this.doubleBackToExitPressedOnce = true
            Toast.makeText(this, getString(R.string.txt_exit_warning), Toast.LENGTH_SHORT).show()

            Handler(Looper.getMainLooper()).postDelayed(
                { doubleBackToExitPressedOnce = false },
                2000
            )
        } else {
            supportFragmentManager.popBackStackImmediate()
        }
    }


    override fun replaceFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
            .replace(binding.fragmentContainer.id, fragment)
            .addToBackStack(fragment.tag)
            .commit()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return false
    }
}